import React, {useState} from 'react';
import styles from './styles.css'




const StepperForm = () => {


  const [activeStep, setActiveStep] = useState('obra');

  const [valores, setValores] = useState([])


  return (
    <div className={styles.container}>
      <div className={styles.titleContainer}>
        <span className={styles.subtexto}>CALCULADOR DE MATERIALES</span>
        <h1 className={styles.title}>Proyectá tu obra sin moverte de casa</h1>
      </div>

      <form className={styles.form}>
        <Obra activeStep = {activeStep} setActiveStep ={setActiveStep} />
        <Etapa activeStep = {activeStep} setActiveStep ={setActiveStep} setValores={setValores} />
        <SubEtapa activeStep = {activeStep} setActiveStep ={setActiveStep} valores = {valores} setValores={setValores} />

      </form>
    </div>
  )
}



const Obra = ({activeStep, setActiveStep}) => {

  const [inputObra, setInputObra] = useState('');

  const [obra, setObra] = useState('');


  const handleObra = (e) => {
    e.preventDefault();
    setActiveStep("etapa");
    setObra(inputObra)

  }

  const handleInputObra = (e) => {
   setInputObra(e.target.value)
  }

  if(activeStep === "obra"){
    return(
      <>
      <div className ={activeStep === "obra" ? styles.containerActive : styles.containerInactive } >
      <div className={styles.containerTitulo}>
     <div className={styles.image}>
        <img src="https://merlinosrl.myvtex.com//arquivos/calculador-paso-1-activo.svg" alt="paso 1"/>
      </div>
      <p>¿Qué tipo de obra querés realizar?</p>
      </div>
      <div className={styles.containerInterno}>

        <div className={styles.containerInput}>
        <label htmlFor="obra" className={styles.label}>Nombre de la obra</label>
        <input type="text" name="obra" onChange = {handleInputObra} value={inputObra} className={styles.input} />
        <button className = {styles.nextButton} onClick={handleObra}>Guardar</button>
        </div>
      </div>

      </div>

      </>
    )

  }
  else{
    return(
      <div className ={activeStep === "obra" ? styles.containerActive : styles.containerInactive }>
        <div className = {obra !== "" ? styles.complete : styles.incomplete } >
        <div className= {styles.linea}></div>
        <img src={obra !== "" ? "https://merlinosrl.myvtex.com//arquivos/calculador-paso-listo.svg" : "https://merlinosrl.myvtex.com//arquivos/calculador-paso-1-inactivo.svg"} alt=""/><p>Obra: {obra} </p>
        </div>

      </div>
    )
  }

}

const Etapa = ({activeStep, setActiveStep, setValores}) => {

  const [inputEtapa, setInputEtapa] = useState('')

  const handleEtapaPrev= (e) => {
    e.preventDefault();
    setActiveStep("obra")

  }



  const handleChangeEtapa = (e) => {
    e.preventDefault();
    setInputEtapa(e.target.value);
    setActiveStep("subetapa");
    setValores([]);
  }


  if(activeStep === "etapa"){
    return(

      <>
      <div className ={activeStep === "etapa" ? styles.containerActive : styles.containerInactive } >
      <div className={styles.containerTitulo}>
      <div className={styles.image}>
          <img src="https://merlinosrl.myvtex.com//arquivos/calculador-paso-2-activo.svg" alt="paso 2"/>
        </div>
      <p>Selección de etapa</p>
      </div>
      <div className={styles.containerExterno}>
      <div onChange={handleChangeEtapa} className={styles.containerEtapas}>
        <div className={styles.column}>
          <div className={styles.option}>
            <input type="radio" name="etapa" id="fundaciones" value="fundaciones"  />
            <label htmlFor="fundaciones">Fundaciones</label>
          </div>

          <div className={styles.option}>
            <input type="radio" name="etapa" id="hormigon" value="Hormigón armado"  />
            <label htmlFor="hormigon">Hormigón armado</label>
          </div>
          <div className={styles.option}>
            <input type="radio" name="etapa" id="mamposteria" value="mamposteria"  />
            <label htmlFor="mamposteria">Mampostería</label>
          </div>
          <div className={styles.option}>
            <input type="radio" name="etapa" id="capa-aisladora" value="capa aisladora"  />
            <label htmlFor="capa-aisladora">Capa aisladora</label>
          </div>
      </div>
      <div className={styles.column}>
          <div className={styles.option}>
            <input type="radio" name="etapa" id="cubierta-techos" value="cubierta de techos"  />
            <label htmlFor="cubierta-techos">Cubierta de techos</label>
          </div>
          <div className={styles.option}>
            <input type="radio" name="etapa" id="revoques" value="revoques"  />
            <label htmlFor="revoques">Revoques</label>
          </div>
          <div className={styles.option}>
            <input type="radio" name="etapa" id="cielorrasos" value="cielorrasos"  />
            <label htmlFor="cielorrasos">Cielorrasos</label>
          </div>
          <div className={styles.option}>
            <input type="radio" name="etapa" id="contrapisos" value="contrapisos"  />
            <label htmlFor="contrapisos">Contrapisos y carpetas</label>
          </div>


      </div>

      </div>

     <div className={styles.imagenEtapas}>
       <img alt="" src="https://merlinosrl.myvtex.com/arquivos/calculador-imagen.png"/>
     </div>
     </div>
      <div className={styles.buttonsContainer}>
        <button className = {styles.prevButton} onClick={handleEtapaPrev}>Volver atrás</button>
        <button className = {styles.nextButton} onClick={handleChangeEtapa}>Guardar</button>
     </div>

      </div>

      </>
    )

  }
  else{
    return(
      <div className ={activeStep === "etapa" ? styles.containerActive : styles.containerInactive }>
         <div className = {inputEtapa !== "" ? styles.complete : styles.incomplete } >
        <img src={inputEtapa !== "" ? "https://merlinosrl.myvtex.com//arquivos/calculador-paso-listo.svg" : "https://merlinosrl.myvtex.com//arquivos/calculador-paso-2-inactivo.svg"} alt=""/>

            <p>Etapa: {inputEtapa} </p>
          </div>
      </div>
    )
  }




}

const SubEtapa = ({activeStep, setActiveStep, valores, setValores}) => {
  const [subetapa, setSubetapa] = useState('');
  const [cantidad, setCantidad] = useState(0);

  const handleValorSubetapa = (e) => {
    setSubetapa(e.target.value)
  }
  const handleCantidad= (e) => {
    setCantidad(e.target.value)
  }

  const handleSingleOpcion = (e)=>{
    e.preventDefault();
    console.log("Etapa:", subetapa, "Cantidad:", cantidad)
    setValores([...valores, {
      "valor": subetapa,
      "cantidad": cantidad
    }])
    console.log(valores)

  }

  const handleSubEtapaPrev= (e) => {
    e.preventDefault();
    setActiveStep("etapa");

  }



  const handleChangeSubEtapa = (e) => {
    e.preventDefault();
    setActiveStep("resultados");
    //console.log(valores)

  }


  if(activeStep === "subetapa"){
    return(

      <>
      <div className ={activeStep === "subetapa" ? styles.containerActive : styles.containerInactive } >
      <div className={styles.containerTitulo}>
     <div className={styles.image}>
        <img src="https://merlinosrl.myvtex.com//arquivos/calculador-paso-3-activo.svg" alt="paso 3"/>
      </div>
      <p>Subetapa</p>
      </div>


      <Combo value="opcion-1"  handleSingleOpcion={handleSingleOpcion} handleValorSubetapa={handleValorSubetapa} handleCantidad={handleCantidad} />
      <Combo value="opcion-2"  handleSingleOpcion={handleSingleOpcion} handleValorSubetapa={handleValorSubetapa} handleCantidad={handleCantidad} />






      <div className={styles.buttonsContainer}>
        <button className = {styles.prevButton} onClick={handleSubEtapaPrev}>Volver atrás</button>

        <button className = {styles.nextButton} onClick={handleChangeSubEtapa}>Guardar</button>
      </div>
      </div>

      </>
    )

  }
  else{
    console.log(valores)
    return(
      <div className ={activeStep === "subetapa" ? styles.containerActive : styles.containerInactive }>
        <div className = {valores.length >= 1 ? styles.complete : styles.incomplete }>

        <img src={valores.length >= 1 ? "https://merlinosrl.myvtex.com//arquivos/calculador-paso-listo.svg" : "https://merlinosrl.myvtex.com//arquivos/calculador-paso-3-inactivo.svg"} alt=""/>


        Subetapa:
        <ul >
          {valores.map( valor => {
            return(
            <li key={valor.valor}><span>{valor.valor}</span><span> {valor.cantidad} metros</span></li>
            )
          })}
        </ul>
        </div>

      </div>
    )
  }




}



const Combo = ({value,  handleSingleOpcion, handleValorSubetapa, handleCantidad}) => {
  const [button, setButton] = useState(false);


  return(
    <form className={styles.subetapa} onSubmit={handleSingleOpcion} >
    <span className={styles.subetapaRow}>
      <input type="checkbox" value={value} id={value} onChange= {handleValorSubetapa} />
      <label htmlFor={value}> {value}</label>
    </span>

    <span className={styles.subetapaRow}>

      <label htmlFor="cantidad">M2</label>

      <input type="number" name="cantidad" onChange= {handleCantidad} />


      <button  onClick={()=>setButton(!button)} className={button? styles.addButtonActive : styles.addButton}>
      {!button ? <span>+</span> :
      <svg width="10" height="8" viewBox="0 0 10 8" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M9.70716 2.07011L4.43174 7.34516C4.04109 7.73588 3.40739 7.73588 3.01637 7.34516L0.29316 4.62175C-0.0976388 4.23103 -0.0976388 3.59726 0.29316 3.20646C0.684034 2.81558 1.31768 2.81558 1.70838 3.20631L3.72426 5.22221L8.29171 0.654738C8.68259 0.263865 9.31628 0.264161 9.70701 0.654738C10.0977 1.04554 10.0977 1.67909 9.70716 2.07011Z" fill="white"/>
      </svg>

      }
        </button>


    </span>


    {button &&  <span>Se agregó correctamente</span>}
  </form>
  )
}


export default StepperForm;
